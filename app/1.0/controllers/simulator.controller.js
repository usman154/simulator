import SimulatorService from '../services/simulator.service';
import Api from '../../../lib/api'
class SimulatorController {

    constructor() { }

    performTelexTransfer(request, response) {
        try {
            let result = SimulatorService.performTelexTransfer();
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }
    listAllCorrespondentBankingCountries(request, response) {
        try {
            let result = SimulatorService.listAllCorrespondentBankingCountries();
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }
    listAllCorrespondentBanks(request, response) {
        try {
            let result = SimulatorService.listAllCorrespondentBanks();
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }
    validateCreateCardlessTransfer(request, response) {
        try {
            let result = SimulatorService.validateCreateCardlessTransfer();
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }
    createCardlessTransfer(request, response) {
        try {
            let result = SimulatorService.createCardlessTransfer();
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }
    cancelCardlessTransfer(request, response) {
        try {
            let result = SimulatorService.cancelCardlessTransfer();
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }
    viewCardlessTransferHistory(request, response) {
        try {
            let result = SimulatorService.viewCardlessTransferHistory();
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }
    verifyQRcode(request, response) {
        try {
            let result = SimulatorService.verifyQRcode();
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }
    getStatementsWithCriteria(request, response) {
        try {
            let result = SimulatorService.getStatementsWithCriteria();
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }
    
}
export default new SimulatorController();