import customerController from '../controllers/simulator.controller';
module.exports = (router) => {
	router.post('/performTelexTransfer/1.0', customerController.performTelexTransfer);
	router.post('/listAllCorrespondentBankingCountries/1.0', customerController.listAllCorrespondentBankingCountries);
	router.post('/listAllCorrespondentBanks/1.0', customerController.listAllCorrespondentBanks);
	router.post('/validateCreateCardlessTransfer/1.0', customerController.validateCreateCardlessTransfer)
	router.post('/createCardlessTransfer/1.0', customerController.createCardlessTransfer)
	router.post('/cancelCardlessTransfer/1.0', customerController.cancelCardlessTransfer)
	router.post('/viewCardlessTransferHistory/1.0', customerController.viewCardlessTransferHistory)
	router.post('/verifyQRcode/1.0', customerController.verifyQRcode)
	router.post('/getStatementsWithCriteria/1.0', customerController.getStatementsWithCriteria)
	
	
};