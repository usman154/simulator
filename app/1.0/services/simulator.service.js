class SimulatorService {
    constructor() { }
    performTelexTransfer() {
        let response = `<S:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
        <env:Header>
           <work:WorkContext xmlns:work="http://oracle.com/weblogic/soap/workarea/">rO0ABXfPAB53ZWJsb2dpYy5hcHAuS0ZIQ29tbW9uQnVzaW5lc3MAAADWAAAAI3dlYmxvZ2ljLndvcmthcmVhLlN0cmluZ1dvcmtDb250ZXh0ABdWMS4yLjQuTWFzdGVyLjExTm92MjAxOQAfd2VibG9naWMuYXBwLktGSEJhbmtpbmdCdXNpbmVzcwAAANYAAAAjd2VibG9naWMud29ya2FyZWEuU3RyaW5nV29ya0NvbnRleHQAG1YxLjguMTIuTWFzdGVyLjE4Tm92MjAxOS54YwAA</work:WorkContext>
        </env:Header>
        <S:Body>
           <ns0:performTelexTransferResponse xmlns:ns0="http://www.openuri.org/" xmlns:ns1="http://www.kfh.com/">
              <ns0:performTelexTransferResult>
                 <ns0:swiftReferenceNo>INT-003-4892071</ns0:swiftReferenceNo>
                 <ns0:customerAvailBal>1144.436</ns0:customerAvailBal>
                 <ns0:customerTotalBal>0.0</ns0:customerTotalBal>
                 <ns0:transferedRate>0.363155</ns0:transferedRate>
                 <ns0:fees>2.25</ns0:fees>
              </ns0:performTelexTransferResult>
           </ns0:performTelexTransferResponse>
        </S:Body>
     </S:Envelope>`
        return response;
    }
    listAllCorrespondentBankingCountries(){
       return `<S:Body>
       <ns1:listAllCorrespondentBankingCountriesResponse xmlns:ns0="http://www.kfh.com/" xmlns:ns1="http://www.openuri.org/">
          <ns1:listAllCorrespondentBankingCountriesResult>
             <ns1:Country>
                <ns1:countryCode>AUS</ns1:countryCode>
                <ns1:englishName>Australia</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>AUT</ns1:countryCode>
                <ns1:englishName>Austria</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>BHR</ns1:countryCode>
                <ns1:englishName>Bahrain</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>BGD</ns1:countryCode>
                <ns1:englishName>Bangladesh</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>BEL</ns1:countryCode>
                <ns1:englishName>Belgium</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>CAN</ns1:countryCode>
                <ns1:englishName>Canada</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>DNK</ns1:countryCode>
                <ns1:englishName>Denmark</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>EGY</ns1:countryCode>
                <ns1:englishName>Egypt</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>FRA</ns1:countryCode>
                <ns1:englishName>France</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>DEU</ns1:countryCode>
                <ns1:englishName>Germany</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>IND</ns1:countryCode>
                <ns1:englishName>India</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>ITA</ns1:countryCode>
                <ns1:englishName>Italy</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>JPN</ns1:countryCode>
                <ns1:englishName>Japan</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>JOR</ns1:countryCode>
                <ns1:englishName>Jordan</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:englishName>Kuwait</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>MYS</ns1:countryCode>
                <ns1:englishName>Malaysia</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>NOR</ns1:countryCode>
                <ns1:englishName>Norway</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>OMN</ns1:countryCode>
                <ns1:englishName>Oman</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>PAK</ns1:countryCode>
                <ns1:englishName>Pakistan</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>PHL</ns1:countryCode>
                <ns1:englishName>Philippine</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>QAT</ns1:countryCode>
                <ns1:englishName>Qatar</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>SAU</ns1:countryCode>
                <ns1:englishName>Saudi Arabia</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>SGP</ns1:countryCode>
                <ns1:englishName>Singapore</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>LKA</ns1:countryCode>
                <ns1:englishName>Srilanka</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>SWE</ns1:countryCode>
                <ns1:englishName>Sweden</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>CHE</ns1:countryCode>
                <ns1:englishName>Switzerland</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>TUR</ns1:countryCode>
                <ns1:englishName>Turkey</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>ARE</ns1:countryCode>
                <ns1:englishName>United Arab Emirates</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>GBR</ns1:countryCode>
                <ns1:englishName>United Kingdom</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
             <ns1:Country>
                <ns1:countryCode>USA</ns1:countryCode>
                <ns1:englishName>United States</ns1:englishName>
                <ns1:ibanLength>0</ns1:ibanLength>
             </ns1:Country>
          </ns1:listAllCorrespondentBankingCountriesResult>
       </ns1:listAllCorrespondentBankingCountriesResponse>
    </S:Body>`
    }
    listAllCorrespondentBanks(){
       return `<S:Body>
       <ns1:listAllCorrespondentBanksResponse xmlns:ns0="http://www.kfh.com/" xmlns:ns1="http://www.openuri.org/">
          <ns1:listAllCorrespondentBanksResult>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1023</ns1:bankId>
                <ns1:bankName>NATIONAL AUSTRALIA BANK LIMITED</ns1:bankName>
                <ns1:swiftCode>NATAAU33XXXX</ns1:swiftCode>
                <ns1:currencyId>4</ns1:currencyId>
                <ns1:currecyNameArabic>الدولار الاسترالي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Australian Dollar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>AUD</ns1:currIsoCode>
                <ns1:countryCode>AUS</ns1:countryCode>
                <ns1:countryNameEnglish>Australia</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1031</ns1:bankId>
                <ns1:bankName>UNICREDIT BANK AUSTRIA AG</ns1:bankName>
                <ns1:swiftCode>BKAUATWWXXXX</ns1:swiftCode>
                <ns1:currencyId>24</ns1:currencyId>
                <ns1:currecyNameArabic>اليورو</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Euro</ns1:currecyNameEnglish>
                <ns1:currIsoCode>EUR</ns1:currIsoCode>
                <ns1:countryCode>AUT</ns1:countryCode>
                <ns1:countryNameEnglish>Austria</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1117</ns1:bankId>
                <ns1:bankName>KUWAIT FINANCE HOUSE BAHRAIN</ns1:bankName>
                <ns1:swiftCode>KFHOBHBMXXXX</ns1:swiftCode>
                <ns1:currencyId>56</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار البحريني</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Bahraini Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>BHD</ns1:currIsoCode>
                <ns1:countryCode>BHR</ns1:countryCode>
                <ns1:countryNameEnglish>Bahrain</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1005</ns1:bankId>
                <ns1:bankName>ISLAMI BANK BANGLADESH LTD.</ns1:bankName>
                <ns1:swiftCode>IBBLBDDHXXXX</ns1:swiftCode>
                <ns1:currencyId>52</ns1:currencyId>
                <ns1:currecyNameArabic>التاكا البنغلاديشي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Bangladeshi Taka</ns1:currecyNameEnglish>
                <ns1:currIsoCode>BDT</ns1:currIsoCode>
                <ns1:countryCode>BGD</ns1:countryCode>
                <ns1:countryNameEnglish>Bangladesh</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1158</ns1:bankId>
                <ns1:bankName>Euroclear Bank SA</ns1:bankName>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>BEL</ns1:countryCode>
                <ns1:countryNameEnglish>Belgium</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>N</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1019</ns1:bankId>
                <ns1:bankName>BANK OF MONTREAL</ns1:bankName>
                <ns1:swiftCode>BOFMCAT2XXXX</ns1:swiftCode>
                <ns1:currencyId>15</ns1:currencyId>
                <ns1:currecyNameArabic>الدولار الكندي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Canadian Dollar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>CAD</ns1:currIsoCode>
                <ns1:countryCode>CAN</ns1:countryCode>
                <ns1:countryNameEnglish>Canada</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1040</ns1:bankId>
                <ns1:bankName>DANSKE BANK A/S</ns1:bankName>
                <ns1:swiftCode>DABADKKKXXXX</ns1:swiftCode>
                <ns1:currencyId>17</ns1:currencyId>
                <ns1:currecyNameArabic>كرون دنماركي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Danish Krone</ns1:currecyNameEnglish>
                <ns1:currIsoCode>DKK</ns1:currIsoCode>
                <ns1:countryCode>DNK</ns1:countryCode>
                <ns1:countryNameEnglish>Denmark</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1002</ns1:bankId>
                <ns1:bankName>FAISAL ISLAMIC BANK OF EGYPT</ns1:bankName>
                <ns1:swiftCode>FIEGEGCXXXXX</ns1:swiftCode>
                <ns1:currencyId>1</ns1:currencyId>
                <ns1:currecyNameArabic>الدولار الأمريكي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>US Dollars</ns1:currecyNameEnglish>
                <ns1:currIsoCode>USD</ns1:currIsoCode>
                <ns1:countryCode>EGY</ns1:countryCode>
                <ns1:countryNameEnglish>Egypt</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1042</ns1:bankId>
                <ns1:bankName>BNP PARIBAS SA</ns1:bankName>
                <ns1:swiftCode>BNPAFRPPXXXX</ns1:swiftCode>
                <ns1:currencyId>24</ns1:currencyId>
                <ns1:currecyNameArabic>اليورو</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Euro</ns1:currecyNameEnglish>
                <ns1:currIsoCode>EUR</ns1:currIsoCode>
                <ns1:countryCode>FRA</ns1:countryCode>
                <ns1:countryNameEnglish>France</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1030</ns1:bankId>
                <ns1:bankName>DEUTSCHE BANK A.G.</ns1:bankName>
                <ns1:swiftCode>DEUTDEFFXXXX</ns1:swiftCode>
                <ns1:currencyId>24</ns1:currencyId>
                <ns1:currecyNameArabic>اليورو</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Euro</ns1:currecyNameEnglish>
                <ns1:currIsoCode>EUR</ns1:currIsoCode>
                <ns1:countryCode>DEU</ns1:countryCode>
                <ns1:countryNameEnglish>Germany</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1161</ns1:bankId>
                <ns1:bankName>KT Bank AG, Germany</ns1:bankName>
                <ns1:swiftCode>KTAGDEFFXXXX</ns1:swiftCode>
                <ns1:currencyId>24</ns1:currencyId>
                <ns1:currecyNameArabic>اليورو</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Euro</ns1:currecyNameEnglish>
                <ns1:currIsoCode>EUR</ns1:currIsoCode>
                <ns1:countryCode>DEU</ns1:countryCode>
                <ns1:countryNameEnglish>Germany</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>N</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1026</ns1:bankId>
                <ns1:bankName>CANARA BANK</ns1:bankName>
                <ns1:swiftCode>CNRBINBBXXXX</ns1:swiftCode>
                <ns1:currencyId>64</ns1:currencyId>
                <ns1:currecyNameArabic>الروبية الهندية</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Indian Rupee</ns1:currecyNameEnglish>
                <ns1:currIsoCode>INR</ns1:currIsoCode>
                <ns1:countryCode>IND</ns1:countryCode>
                <ns1:countryNameEnglish>India</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1034</ns1:bankId>
                <ns1:bankName>INTESA SANPAOLO SPA</ns1:bankName>
                <ns1:swiftCode>BCITITMMXXXX</ns1:swiftCode>
                <ns1:currencyId>24</ns1:currencyId>
                <ns1:currecyNameArabic>اليورو</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Euro</ns1:currecyNameEnglish>
                <ns1:currIsoCode>EUR</ns1:currIsoCode>
                <ns1:countryCode>ITA</ns1:countryCode>
                <ns1:countryNameEnglish>Italy</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1014</ns1:bankId>
                <ns1:bankName>MUFG BANK, LTD.</ns1:bankName>
                <ns1:swiftCode>BOTKJPJTXXXX</ns1:swiftCode>
                <ns1:currencyId>8</ns1:currencyId>
                <ns1:currecyNameArabic>الين الياباني</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Japanese Yen</ns1:currecyNameEnglish>
                <ns1:currIsoCode>JPY</ns1:currIsoCode>
                <ns1:countryCode>JPN</ns1:countryCode>
                <ns1:countryNameEnglish>Japan</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1039</ns1:bankId>
                <ns1:bankName>JORDAN ISLAMIC BANK FOR FINANCE &amp; INV.</ns1:bankName>
                <ns1:swiftCode>JIBAJOAMXXXX</ns1:swiftCode>
                <ns1:currencyId>60</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الأردني</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Jordanian Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>JOD</ns1:currIsoCode>
                <ns1:countryCode>JOR</ns1:countryCode>
                <ns1:countryNameEnglish>Jordan</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1130</ns1:bankId>
                <ns1:bankName>ABU DHABI NATIONAL BANK, KUWAIT</ns1:bankName>
                <ns1:swiftCode>NBADKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1104</ns1:bankId>
                <ns1:bankName>AHLI UNITED BANK KSC</ns1:bankName>
                <ns1:swiftCode>BKMEKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1140</ns1:bankId>
                <ns1:bankName>AL RAJHI BANK KUWAIT</ns1:bankName>
                <ns1:swiftCode>RJHIKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1107</ns1:bankId>
                <ns1:bankName>AL-AHLI BANK OF KUWAIT</ns1:bankName>
                <ns1:swiftCode>ABKKKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1141</ns1:bankId>
                <ns1:bankName>BANK MUSCAT KUWAIT</ns1:bankName>
                <ns1:swiftCode>BMUSKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1106</ns1:bankId>
                <ns1:bankName>BANK OF BAHRAIN &amp; KUWAIT</ns1:bankName>
                <ns1:swiftCode>BBKUKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1131</ns1:bankId>
                <ns1:bankName>BNP PARIBAS, KUWAIT</ns1:bankName>
                <ns1:swiftCode>BNPAKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1128</ns1:bankId>
                <ns1:bankName>BOUBYAN BANK</ns1:bankName>
                <ns1:swiftCode>BBYNKWKW</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1105</ns1:bankId>
                <ns1:bankName>BURGAN BANK</ns1:bankName>
                <ns1:swiftCode>BRGNKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1100</ns1:bankId>
                <ns1:bankName>CENTRAL BANK OF KUWAIT</ns1:bankName>
                <ns1:swiftCode>CBKUKWKTXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1135</ns1:bankId>
                <ns1:bankName>CITIBANK NA KUWAIT</ns1:bankName>
                <ns1:swiftCode>CITIKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1102</ns1:bankId>
                <ns1:bankName>COMMERCIAL BANK OF KUWAIT</ns1:bankName>
                <ns1:swiftCode>COMBKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1137</ns1:bankId>
                <ns1:bankName>DOHA BANK KUWAIT</ns1:bankName>
                <ns1:swiftCode>DOHBKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1129</ns1:bankId>
                <ns1:bankName>HSBC BANK MIDDLE EAST, KUWAIT</ns1:bankName>
                <ns1:swiftCode>HBMEKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1162</ns1:bankId>
                <ns1:bankName>Industrial and Commercial Bank of China</ns1:bankName>
                <ns1:swiftCode>ICBKKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1108</ns1:bankId>
                <ns1:bankName>INDUSTRIAL BANK OF KUWAIT</ns1:bankName>
                <ns1:swiftCode>IBKUKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1109</ns1:bankId>
                <ns1:bankName>KUWAIT INTERNATIONAL BANK</ns1:bankName>
                <ns1:swiftCode>KWIBKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1139</ns1:bankId>
                <ns1:bankName>MASHREQ BANK KUWAIT</ns1:bankName>
                <ns1:swiftCode>MSHQKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1101</ns1:bankId>
                <ns1:bankName>NATIONAL BANK OF KUWAIT</ns1:bankName>
                <ns1:swiftCode>NBOKKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1143</ns1:bankId>
                <ns1:bankName>QATAR NATIONAL BANK - KUWAIT BRANCH</ns1:bankName>
                <ns1:swiftCode>QNBAKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1103</ns1:bankId>
                <ns1:bankName>THE GULF BANK</ns1:bankName>
                <ns1:swiftCode>GULBKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1151</ns1:bankId>
                <ns1:bankName>UNION NATIONAL BANK - KUWAIT</ns1:bankName>
                <ns1:swiftCode>UNBEKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1144</ns1:bankId>
                <ns1:bankName>WARBA BANK</ns1:bankName>
                <ns1:swiftCode>WRBAKWKWXXXX</ns1:swiftCode>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>KWT</ns1:countryCode>
                <ns1:countryNameEnglish>Kuwait</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1132</ns1:bankId>
                <ns1:bankName>KUWAIT FINANCE HOUSE (MALAYSIA) BERHARD</ns1:bankName>
                <ns1:swiftCode>KFHOMYKLXXXX</ns1:swiftCode>
                <ns1:currencyId>21</ns1:currencyId>
                <ns1:currecyNameArabic>الرينجت الماليزي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Malaysian Ringgit</ns1:currecyNameEnglish>
                <ns1:currIsoCode>MYR</ns1:currIsoCode>
                <ns1:countryCode>MYS</ns1:countryCode>
                <ns1:countryNameEnglish>Malaysia</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1148</ns1:bankId>
                <ns1:bankName>DANSKE BANK A/S - NORWAY</ns1:bankName>
                <ns1:swiftCode>DABANO22XXXX</ns1:swiftCode>
                <ns1:currencyId>27</ns1:currencyId>
                <ns1:currecyNameArabic>Norway - Kroner</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Norway - Kroner</ns1:currecyNameEnglish>
                <ns1:currIsoCode>NOK</ns1:currIsoCode>
                <ns1:countryCode>NOR</ns1:countryCode>
                <ns1:countryNameEnglish>Norway</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>N</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1037</ns1:bankId>
                <ns1:bankName>NATIONAL BANK OF OMAN</ns1:bankName>
                <ns1:swiftCode>NBOMOMRXXXXX</ns1:swiftCode>
                <ns1:currencyId>51</ns1:currencyId>
                <ns1:currecyNameArabic>الريال العماني</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Omani Rial</ns1:currecyNameEnglish>
                <ns1:currIsoCode>OMR</ns1:currIsoCode>
                <ns1:countryCode>OMN</ns1:countryCode>
                <ns1:countryNameEnglish>Oman</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1118</ns1:bankId>
                <ns1:bankName>HABIB BANK LIMITED</ns1:bankName>
                <ns1:swiftCode>HABBPKKAXXXX</ns1:swiftCode>
                <ns1:currencyId>50</ns1:currencyId>
                <ns1:currecyNameArabic>الروبية الباكستانية</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Pakistani Rupee</ns1:currecyNameEnglish>
                <ns1:currIsoCode>PKR</ns1:currIsoCode>
                <ns1:countryCode>PAK</ns1:countryCode>
                <ns1:countryNameEnglish>Pakistan</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1007</ns1:bankId>
                <ns1:bankName>MCB BANK LIMITED</ns1:bankName>
                <ns1:swiftCode>MUCBPKKAXXXX</ns1:swiftCode>
                <ns1:currencyId>50</ns1:currencyId>
                <ns1:currecyNameArabic>الروبية الباكستانية</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Pakistani Rupee</ns1:currecyNameEnglish>
                <ns1:currIsoCode>PKR</ns1:currIsoCode>
                <ns1:countryCode>PAK</ns1:countryCode>
                <ns1:countryNameEnglish>Pakistan</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1021</ns1:bankId>
                <ns1:bankName>BANCO DE ORO-EPCI, INC.</ns1:bankName>
                <ns1:swiftCode>BNORPHMMXXXX</ns1:swiftCode>
                <ns1:currencyId>1</ns1:currencyId>
                <ns1:currecyNameArabic>الدولار الأمريكي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>US Dollars</ns1:currecyNameEnglish>
                <ns1:currIsoCode>USD</ns1:currIsoCode>
                <ns1:countryCode>PHL</ns1:countryCode>
                <ns1:countryNameEnglish>Philippine</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1022</ns1:bankId>
                <ns1:bankName>BANCO DE ORO-EPCI, INC.</ns1:bankName>
                <ns1:swiftCode>BNORPHMMXXXX</ns1:swiftCode>
                <ns1:currencyId>49</ns1:currencyId>
                <ns1:currecyNameArabic>البيزو الفلبيني</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Philippine Peso</ns1:currecyNameEnglish>
                <ns1:currIsoCode>PHP</ns1:currIsoCode>
                <ns1:countryCode>PHL</ns1:countryCode>
                <ns1:countryNameEnglish>Philippine</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1001</ns1:bankId>
                <ns1:bankName>QATAR ISLAMIC BANK</ns1:bankName>
                <ns1:swiftCode>QISBQAQAXXXX</ns1:swiftCode>
                <ns1:currencyId>36</ns1:currencyId>
                <ns1:currecyNameArabic>الريال القطري</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Qatar Riyal</ns1:currecyNameEnglish>
                <ns1:currIsoCode>QAR</ns1:currIsoCode>
                <ns1:countryCode>QAT</ns1:countryCode>
                <ns1:countryNameEnglish>Qatar</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1006</ns1:bankId>
                <ns1:bankName>ALRAJHI BANK</ns1:bankName>
                <ns1:swiftCode>RJHISARIXXXX</ns1:swiftCode>
                <ns1:currencyId>37</ns1:currencyId>
                <ns1:currecyNameArabic>الريال السعودي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Saudi Arabian Riyal</ns1:currecyNameEnglish>
                <ns1:currIsoCode>SAR</ns1:currIsoCode>
                <ns1:countryCode>SAU</ns1:countryCode>
                <ns1:countryNameEnglish>Saudi Arabia</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1154</ns1:bankId>
                <ns1:bankName>SAMBA FINANCIAL GROUP</ns1:bankName>
                <ns1:swiftCode>SAMBSARIXXXX</ns1:swiftCode>
                <ns1:currencyId>37</ns1:currencyId>
                <ns1:currecyNameArabic>الريال السعودي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Saudi Arabian Riyal</ns1:currecyNameEnglish>
                <ns1:currIsoCode>SAR</ns1:currIsoCode>
                <ns1:countryCode>SAU</ns1:countryCode>
                <ns1:countryNameEnglish>Saudi Arabia</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1133</ns1:bankId>
                <ns1:bankName>DBS BANK LTD.</ns1:bankName>
                <ns1:swiftCode>DBSSSGSGXXXX</ns1:swiftCode>
                <ns1:currencyId>13</ns1:currencyId>
                <ns1:currecyNameArabic>الدولار السنغافوري</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Singapore Dollar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>SGD</ns1:currIsoCode>
                <ns1:countryCode>SGP</ns1:countryCode>
                <ns1:countryNameEnglish>Singapore</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1008</ns1:bankId>
                <ns1:bankName>COMMERCIAL BANK OF CEYLON LTD.</ns1:bankName>
                <ns1:swiftCode>CCEYLKLXXXXX</ns1:swiftCode>
                <ns1:currencyId>41</ns1:currencyId>
                <ns1:currecyNameArabic>الروبية السيريلانكية</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Sri Lanka Rupee</ns1:currecyNameEnglish>
                <ns1:currIsoCode>LKR</ns1:currIsoCode>
                <ns1:countryCode>LKA</ns1:countryCode>
                <ns1:countryNameEnglish>Srilanka</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1020</ns1:bankId>
                <ns1:bankName>SKANDINAVISKA ENSKILDA BANKEN</ns1:bankName>
                <ns1:swiftCode>ESSESESSXXXX</ns1:swiftCode>
                <ns1:currencyId>28</ns1:currencyId>
                <ns1:currecyNameArabic>الكرونا السويدية</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Swedish Krona</ns1:currecyNameEnglish>
                <ns1:currIsoCode>SEK</ns1:currIsoCode>
                <ns1:countryCode>SWE</ns1:countryCode>
                <ns1:countryNameEnglish>Sweden</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1159</ns1:bankId>
                <ns1:bankName>BSI AS</ns1:bankName>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>CHE</ns1:countryCode>
                <ns1:countryNameEnglish>Switzerland</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>N</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1155</ns1:bankId>
                <ns1:bankName>CREDIT SUISSE AG ZURICH</ns1:bankName>
                <ns1:swiftCode>CRESCHZZ80A</ns1:swiftCode>
                <ns1:currencyId>1</ns1:currencyId>
                <ns1:currecyNameArabic>الدولار الأمريكي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>US Dollars</ns1:currecyNameEnglish>
                <ns1:currIsoCode>USD</ns1:currIsoCode>
                <ns1:countryCode>CHE</ns1:countryCode>
                <ns1:countryNameEnglish>Switzerland</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>N</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1157</ns1:bankId>
                <ns1:bankName>CREDIT SUISSE AG ZURICH</ns1:bankName>
                <ns1:swiftCode>CRESCHZZXXXX</ns1:swiftCode>
                <ns1:currencyId>10</ns1:currencyId>
                <ns1:currecyNameArabic>الفرنك السويسري</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Swiss Franc</ns1:currecyNameEnglish>
                <ns1:currIsoCode>CHF</ns1:currIsoCode>
                <ns1:countryCode>CHE</ns1:countryCode>
                <ns1:countryNameEnglish>Switzerland</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1156</ns1:bankId>
                <ns1:bankName>CREDIT SUISSE AG ZURICH</ns1:bankName>
                <ns1:swiftCode>CRESCHZZ80A</ns1:swiftCode>
                <ns1:currencyId>24</ns1:currencyId>
                <ns1:currecyNameArabic>اليورو</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Euro</ns1:currecyNameEnglish>
                <ns1:currIsoCode>EUR</ns1:currIsoCode>
                <ns1:countryCode>CHE</ns1:countryCode>
                <ns1:countryNameEnglish>Switzerland</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1160</ns1:bankId>
                <ns1:bankName>Julius Baer</ns1:bankName>
                <ns1:currencyId>40</ns1:currencyId>
                <ns1:currecyNameArabic>الدينار الكويتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Kuwaiti Dinar</ns1:currecyNameEnglish>
                <ns1:currIsoCode>KWD</ns1:currIsoCode>
                <ns1:countryCode>CHE</ns1:countryCode>
                <ns1:countryNameEnglish>Switzerland</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>N</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1150</ns1:bankId>
                <ns1:bankName>KUWAIT TURKISH PARTICIPATION BANK</ns1:bankName>
                <ns1:swiftCode>KTEFTRISXXXX</ns1:swiftCode>
                <ns1:currencyId>54</ns1:currencyId>
                <ns1:currecyNameArabic>Turkish Lira</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Turkish Lira</ns1:currecyNameEnglish>
                <ns1:currIsoCode>TRY</ns1:currIsoCode>
                <ns1:countryCode>TUR</ns1:countryCode>
                <ns1:countryNameEnglish>Turkey</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>N</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1009</ns1:bankId>
                <ns1:bankName>KUWAIT TURKISH PARTICIPATION BANK INC.</ns1:bankName>
                <ns1:swiftCode>KTEFTRISXXXX</ns1:swiftCode>
                <ns1:currencyId>1</ns1:currencyId>
                <ns1:currecyNameArabic>الدولار الأمريكي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>US Dollars</ns1:currecyNameEnglish>
                <ns1:currIsoCode>USD</ns1:currIsoCode>
                <ns1:countryCode>TUR</ns1:countryCode>
                <ns1:countryNameEnglish>Turkey</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1004</ns1:bankId>
                <ns1:bankName>DUBAI ISLAMIC BANK</ns1:bankName>
                <ns1:swiftCode>DUIBAEADXXXX</ns1:swiftCode>
                <ns1:currencyId>61</ns1:currencyId>
                <ns1:currecyNameArabic>الدرهم الإماراتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>UAE Dirhem</ns1:currecyNameEnglish>
                <ns1:currIsoCode>AED</ns1:currIsoCode>
                <ns1:countryCode>ARE</ns1:countryCode>
                <ns1:countryNameEnglish>United Arab Emirates</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1027</ns1:bankId>
                <ns1:bankName>SHARJAH ISLAMIC BANK</ns1:bankName>
                <ns1:swiftCode>NBSHAEASXXXX</ns1:swiftCode>
                <ns1:currencyId>61</ns1:currencyId>
                <ns1:currecyNameArabic>الدرهم الإماراتي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>UAE Dirhem</ns1:currecyNameEnglish>
                <ns1:currIsoCode>AED</ns1:currIsoCode>
                <ns1:countryCode>ARE</ns1:countryCode>
                <ns1:countryNameEnglish>United Arab Emirates</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1015</ns1:bankId>
                <ns1:bankName>AHLI UINITED BANK (UK) PLC</ns1:bankName>
                <ns1:swiftCode>UBKLGB2LXXXX</ns1:swiftCode>
                <ns1:currencyId>9</ns1:currencyId>
                <ns1:currecyNameArabic>الجنيه الاسترليني</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>British Pound Sterling</ns1:currecyNameEnglish>
                <ns1:currIsoCode>GBP</ns1:currIsoCode>
                <ns1:countryCode>GBR</ns1:countryCode>
                <ns1:countryNameEnglish>United Kingdom</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1152</ns1:bankId>
                <ns1:bankName>BANK JULIUS BAER &amp; CO. GUERNSEY</ns1:bankName>
                <ns1:swiftCode>BAERGGSPXXXX</ns1:swiftCode>
                <ns1:currencyId>1</ns1:currencyId>
                <ns1:currecyNameArabic>الدولار الأمريكي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>US Dollars</ns1:currecyNameEnglish>
                <ns1:currIsoCode>USD</ns1:currIsoCode>
                <ns1:countryCode>GBR</ns1:countryCode>
                <ns1:countryNameEnglish>United Kingdom</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>N</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1153</ns1:bankId>
                <ns1:bankName>BANK JULIUS BAER &amp; CO. GUERNSEY</ns1:bankName>
                <ns1:swiftCode>BAERGGSPXXXX</ns1:swiftCode>
                <ns1:currencyId>24</ns1:currencyId>
                <ns1:currecyNameArabic>اليورو</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>Euro</ns1:currecyNameEnglish>
                <ns1:currIsoCode>EUR</ns1:currIsoCode>
                <ns1:countryCode>GBR</ns1:countryCode>
                <ns1:countryNameEnglish>United Kingdom</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>Y</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1017</ns1:bankId>
                <ns1:bankName>BARCLAYS BANK PLC</ns1:bankName>
                <ns1:swiftCode>BARCGB22XXXX</ns1:swiftCode>
                <ns1:currencyId>9</ns1:currencyId>
                <ns1:currecyNameArabic>الجنيه الاسترليني</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>British Pound Sterling</ns1:currecyNameEnglish>
                <ns1:currIsoCode>GBP</ns1:currIsoCode>
                <ns1:countryCode>GBR</ns1:countryCode>
                <ns1:countryNameEnglish>United Kingdom</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1114</ns1:bankId>
                <ns1:bankName>HSBC BANK PLC</ns1:bankName>
                <ns1:swiftCode>MIDLGB22XXXX</ns1:swiftCode>
                <ns1:currencyId>9</ns1:currencyId>
                <ns1:currecyNameArabic>الجنيه الاسترليني</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>British Pound Sterling</ns1:currecyNameEnglish>
                <ns1:currIsoCode>GBP</ns1:currIsoCode>
                <ns1:countryCode>GBR</ns1:countryCode>
                <ns1:countryNameEnglish>United Kingdom</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1142</ns1:bankId>
                <ns1:bankName>STANDARD CHARTERED BANK LONDON</ns1:bankName>
                <ns1:swiftCode>SCBLGB2LXXXX</ns1:swiftCode>
                <ns1:currencyId>9</ns1:currencyId>
                <ns1:currecyNameArabic>الجنيه الاسترليني</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>British Pound Sterling</ns1:currecyNameEnglish>
                <ns1:currIsoCode>GBP</ns1:currIsoCode>
                <ns1:countryCode>GBR</ns1:countryCode>
                <ns1:countryNameEnglish>United Kingdom</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1012</ns1:bankId>
                <ns1:bankName>CITI BANK N.A (NEW YORK)</ns1:bankName>
                <ns1:swiftCode>CITIUS33XXXX</ns1:swiftCode>
                <ns1:currencyId>1</ns1:currencyId>
                <ns1:currecyNameArabic>الدولار الأمريكي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>US Dollars</ns1:currecyNameEnglish>
                <ns1:currIsoCode>USD</ns1:currIsoCode>
                <ns1:countryCode>USA</ns1:countryCode>
                <ns1:countryNameEnglish>United States</ns1:countryNameEnglish>
             </ns1:CorrespondentBankingBanksDTO>
             <ns1:CorrespondentBankingBanksDTO>
                <ns1:bankId>1146</ns1:bankId>
                <ns1:bankName>STANDARD CHARTERED BANK NEW YORK</ns1:bankName>
                <ns1:swiftCode>SCBLUS33XXXX</ns1:swiftCode>
                <ns1:currencyId>1</ns1:currencyId>
                <ns1:currecyNameArabic>الدولار الأمريكي</ns1:currecyNameArabic>
                <ns1:currecyNameEnglish>US Dollars</ns1:currecyNameEnglish>
                <ns1:currIsoCode>USD</ns1:currIsoCode>
                <ns1:countryCode>USA</ns1:countryCode>
                <ns1:countryNameEnglish>United States</ns1:countryNameEnglish>
                <ns1:mandatoryIBAN>N</ns1:mandatoryIBAN>
             </ns1:CorrespondentBankingBanksDTO>
          </ns1:listAllCorrespondentBanksResult>
       </ns1:listAllCorrespondentBanksResponse>
    </S:Body>`
    }
    validateCreateCardlessTransfer(){
       return `<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
       <S:Header>
          <work:WorkContext xmlns:work="http://oracle.com/weblogic/soap/workarea/">rO0ABXdhACJ3ZWJsb2dpYy5hcHAuS0ZIQmFua2luZ0J1c2luZXNzRUFSAAAA1gAAACN3ZWJsb2dpYy53b3JrYXJlYS5TdHJpbmdXb3JrQ29udGV4dAAOVjEuMy4xMC5KdWx5MjMAAA==</work:WorkContext>
       </S:Header>
       <S:Body>
          <ns0:validateCreateCardlessTransferResponse xmlns:ns0="http://www.kfh.com/">
             <validateCreateCardlessTransfer>
                <isSuccessful>true</isSuccessful>
                <statusCode>0</statusCode>
             </validateCreateCardlessTransfer>
          </ns0:validateCreateCardlessTransferResponse>
       </S:Body>
    </S:Envelope>`
    }
    createCardlessTransfer(){
       return `<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
       <S:Header>
          <work:WorkContext xmlns:work="http://oracle.com/weblogic/soap/workarea/">rO0ABXe3ACJ3ZWJsb2dpYy5hcHAuS0ZIQmFua2luZ0J1c2luZXNzRUFSAAAA1gAAACN3ZWJsb2dpYy53b3JrYXJlYS5TdHJpbmdXb3JrQ29udGV4dAAOVjEuMy4xMC5KdWx5MjMAIXdlYmxvZ2ljLmFwcC5LRkhDb21tb25CdXNpbmVzc0VBUgAAANYAAAAjd2VibG9naWMud29ya2FyZWEuU3RyaW5nV29ya0NvbnRleHQABlYxLjAuNwAA</work:WorkContext>
       </S:Header>
       <S:Body>
          <ns0:createCardlessTransferResponse xmlns:ns0="http://www.kfh.com/">
             <createCardlessTransfer>
                <isSuccessful>true</isSuccessful>
                <statusCode>0</statusCode>
             </createCardlessTransfer>
          </ns0:createCardlessTransferResponse>
       </S:Body>
    </S:Envelope>`
    }
    cancelCardlessTransfer(){
       return `<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
       <S:Header>
          <work:WorkContext xmlns:work="http://oracle.com/weblogic/soap/workarea/">rO0ABXdhACJ3ZWJsb2dpYy5hcHAuS0ZIQmFua2luZ0J1c2luZXNzRUFSAAAA1gAAACN3ZWJsb2dpYy53b3JrYXJlYS5TdHJpbmdXb3JrQ29udGV4dAAOVjEuMy4xMC5KdWx5MjMAAA==</work:WorkContext>
       </S:Header>
       <S:Body>
          <ns0:cancelCardlessTransferResponse xmlns:ns0="http://www.kfh.com/">
             <cancelCardlessTransfer>
                <isSuccessful>false</isSuccessful>
                <statusCode>CLTP-07</statusCode>
                <statusDescription>Cardless transfer is already cancelled/expired/completed.</statusDescription>
             </cancelCardlessTransfer>
          </ns0:cancelCardlessTransferResponse>
       </S:Body>
    </S:Envelope>`
    }
    viewCardlessTransferHistory(){
       return `<S:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
       <env:Header>
          <work:WorkContext xmlns:work="http://oracle.com/weblogic/soap/workarea/">rO0ABXdaACJ3ZWJsb2dpYy5hcHAuS0ZIQmFua2luZ0J1c2luZXNzRUFSAAAA1gAAACN3ZWJsb2dpYy53b3JrYXJlYS5TdHJpbmdXb3JrQ29udGV4dAAHVjEuMy4yMAAA</work:WorkContext>
       </env:Header>
       <S:Body>
          <ns0:viewCardlessTransferHistoryResponse xmlns:ns0="http://www.kfh.com/">
             <viewCardlessTransferHistory>
                <listViewCardlessTransferHistoryDTO>
                   <amount>10.0</amount>
                   <beneficiaryName></beneficiaryName>
                   <creationTime>2018-06-11T14:30:22.18+03:00</creationTime>
                   <debitAccountNo>011021064355</debitAccountNo>
                   <removeHold>true</removeHold>
                   <status>Pending</status>
                   <transferID>3027</transferID>
                   <transferValue>751FCFD6423D431</transferValue>
                </listViewCardlessTransferHistoryDTO>
                <listViewCardlessTransferHistoryDTO>
                   <amount>10.0</amount>
                   <beneficiaryName></beneficiaryName>
                   <creationTime>2018-06-11T14:34:23.17+03:00</creationTime>
                   <debitAccountNo>011021064355</debitAccountNo>
                   <removeHold>true</removeHold>
                   <status>Pending</status>
                   <transferID>3028</transferID>
                   <transferValue>33112B96DAC90E9</transferValue>
                </listViewCardlessTransferHistoryDTO>
             </viewCardlessTransferHistory>
          </ns0:viewCardlessTransferHistoryResponse>
       </S:Body>
    </S:Envelope>`
    }
    verifyQRcode(){
       return `<S:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
       <env:Header>
          <work:WorkContext xmlns:work="http://oracle.com/weblogic/soap/workarea/">rO0ABXdaACJ3ZWJsb2dpYy5hcHAuS0ZIQmFua2luZ0J1c2luZXNzRUFSAAAA1gAAACN3ZWJsb2dpYy53b3JrYXJlYS5TdHJpbmdXb3JrQ29udGV4dAAHVjEuMy4yMAAA</work:WorkContext>
       </env:Header>
       <S:Body>
          <ns0:verifyQRcodeResponse xmlns:ns0="http://www.kfh.com/">
             <performCardlessTransactionBasedOnTypeResult>
                <isSuccessful>true</isSuccessful>
                <statusCode>0</statusCode>
             </performCardlessTransactionBasedOnTypeResult>
          </ns0:verifyQRcodeResponse>
       </S:Body>
    </S:Envelope>`
    }
    getStatementsWithCriteria(){
       return `<S:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">

       <env:Header>
    
          <work:WorkContext xmlns:work="http://oracle.com/weblogic/soap/workarea/">rO0ABXdrAB93ZWJsb2dpYy5hcHAuS0ZIQmFua2luZ0J1c2luZXNzAAAA1gAAACN3ZWJsb2dpYy53b3JrYXJlYS5TdHJpbmdXb3JrQ29udGV4dAAbVjEuOC4xMi5NYXN0ZXIuMjBPY3QyMDE5LnhjAAA=</work:WorkContext>
    
       </env:Header>
    
       <S:Body>
    
          <ns0:getStatementsWithCriteriaResponse xmlns:ns0="http://www.openuri.org/" xmlns:ns1="http://www.kfh.com/">
    
             <ns0:getStatementsWithCriteriaResult>
    
                <ns0:acctNo>011010401440</ns0:acctNo>
    
                <ns0:totalNoOfTransactions>2</ns0:totalNoOfTransactions>
    
                <ns0:pageNoOfTransactions>2</ns0:pageNoOfTransactions>
    
                <ns0:queryKey>55757</ns0:queryKey>
    
                <ns0:StatementTransactions>
    
                   <ns0:item xsi:type="ns0:StatementTransaction" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    
                      <ns0:date>2017-06-04T11:26:06+03:00</ns0:date>
    
                      <ns0:creditAmount>3500.0</ns0:creditAmount>
    
                      <ns0:debitAmount>0.0</ns0:debitAmount>
    
                      <ns0:bal>4204.517</ns0:bal>
    
                      <ns0:description>MASKED DESCRIPTION</ns0:description>
    
                      <ns0:channel_ar_desc>تحويل مالى - انترنت</ns0:channel_ar_desc>
    
                      <ns0:channel_en_desc>Fund Transfer-Internet</ns0:channel_en_desc>
    
                      <ns0:channelId>6015</ns0:channelId>
    
                      <ns0:ptid>1582864163</ns0:ptid>
    
                      <ns0:reference_no>00001030000416992595</ns0:reference_no>
    
                   </ns0:item>
    
                   <ns0:item xsi:type="ns0:StatementTransaction" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    
                      <ns0:date>2017-06-05T10:02:43+03:00</ns0:date>
    
                      <ns0:creditAmount>0.0</ns0:creditAmount>
    
                      <ns0:debitAmount>3000.0</ns0:debitAmount>
    
                      <ns0:bal>1204.517</ns0:bal>
    
                      <ns0:description>MASKED DESCRIPTION</ns0:description>
    
                      <ns0:chequeNo>1497</ns0:chequeNo>
    
                      <ns0:channel_ar_desc>شيك بيت التمويل</ns0:channel_ar_desc>
    
                      <ns0:channel_en_desc>KFH Chq</ns0:channel_en_desc>
    
                      <ns0:channelId>5087</ns0:channelId>
    
                      <ns0:ptid>1583153477</ns0:ptid>
    
                      <ns0:reference_no>00001030000417126428</ns0:reference_no>
    
                   </ns0:item>
    
                </ns0:StatementTransactions>
    
             </ns0:getStatementsWithCriteriaResult>
    
          </ns0:getStatementsWithCriteriaResponse>
    
       </S:Body>
    
    </S:Envelope>`
    }
}

export default new SimulatorService();
